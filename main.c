#include "tests.h"
#include <dislib.h>

static int ADC_Result = 0;
static uint8_t xdata = 0;
static uint8_t ydata = 0;
static uint8_t zdata = 0;
uint8_t state = 1;
int brightness = 0;
uint32_t i;
uint8_t dir = 1;
uint16_t value;

#define MEMS_PASSCONDITION   15
#define ABS(a)(a < 0 ? -a: a)
#define  THRESHOLD 6

void BasicGPIOTest()
{
	EnableGPIOClok(GPIOD);
	InitLeds();
	InitUserButton();

	int normalButtonState = GetButtonState();

	    while(1)
	    {
	    	if (GetButtonState() != normalButtonState)
	    		ResetLeds();
	    	else
	    		SetLeds();
	    }
}

void EXTI0_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line0) != RESET)
	{

		Delay(25);

		if (state)
		{
			SetLeds();
			state = 0;
		}
		else
		{

			ResetLeds();
			state = 1;
		}
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}

void BasicNVICbuttonTest()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	EnableGPIOClok(GPIOA);
	EnableGPIOClok(GPIOD);
	InitLeds();
	InitUserButton();
	SetUpDelayTimer();
	SetupNVIC(ENABLE, EXTI0_IRQn, 0, 0);
	SetupEXTI(ENABLE, GPIO_Pin_0, EXTI_Mode_Interrupt, EXTI_Trigger_Rising);
	ConnectButtonToEXIT();


	while(1){}
}

void BasicTimerTest()
{
	EnableGPIOClok(GPIOD);
	InitLeds();
	SetLeds();

	InitTimer(TIM3);
	SetTimer(TIM3, 1000, 1000);
	EnableTimer(TIM3);

	uint8_t LedState = 1;

	while (1)
	{
		if (CheckTimer(TIM3) != RESET)
		{
			if (LedState)
			{
				ResetLeds();
				LedState = 0;
			}
			else
			{
				SetLeds();
				LedState = 1;
			}
			ResetTimer(TIM3);
		}
	}
}

uint8_t State = 1;

void TIM3_IRQHandler(void)
{

	if (State)
	{
		ResetLeds();
		State = 0;
	}
	else
	{
		SetLeds();
		State = 1;
	}
	ResetTimer(TIM3);

	ClearTimerPendingBit(TIM3);
}

void BasicTimerNvicTest()
{
	EnableGPIOClok(GPIOD);
	InitLeds();
	InitTimer(TIM3);
	SetTimer(TIM3, 1000, 1000);
	EnableTimer(TIM3);
	EnableUpdateInt(TIM3);
	SetupNVIC(ENABLE, TIM3_IRQn, 0, 0);
	EnableIRQn(TIM3_IRQn);

	volatile int a;
	while(1)
	{
		a++;
		a--;
		a = 10;
	}
}

void BasicPWMTest()
{
	InitTimer(TIM4);
	EnableGPIOClok(GPIOD);
	InitLedsPWM();
	SetTimer(TIM4, 1, 3000);

	SetChannelPWM(TIM4, 1);
	SetChannelPWM(TIM4, 2);
	SetChannelPWM(TIM4, 3);
	SetChannelPWM(TIM4, 4);

	EnableTimerPreload(TIM4);

	EnableTimer(TIM4);

	uint8_t dir = 1;
	SetPWM(TIM4, 3000, 4);

	while(1)
	{
		if (dir)
			brightness++;
		else
			brightness--;

		if(brightness <= 0)
			dir = 1;

		if (brightness >= 1500)
			dir = 0;

		SetPWM(TIM4, brightness, 1);
		SetPWM(TIM4, brightness, 2);
		SetPWM(TIM4, brightness, 3);
	}
}


void BasicDACTest()
{

	SystemInit();
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef  ADC_InitStructure;
	DAC_InitTypeDef  DAC_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD , ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);


	//Set GPIOA4 as DAC output
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	//Set GPIOA1 as ADC input
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	//ADC resolution
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_3Cycles);

	ADC_Cmd(ADC1, ENABLE);

	DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
	DAC_Init(DAC_Channel_1, &DAC_InitStructure);


	DAC_Cmd(DAC_Channel_1, ENABLE);

	while(1)
	{
		DAC_SetChannel1Data(DAC_Align_12b_R, value);


		if (1 == dir)
			value += 1;

		if (0 == dir)
			value -= 1;

		if (value == 0)
			dir = 1;

		if (value > 0xfff)
			dir = 0;

		ADC_SoftwareStartConv(ADC1);

		while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
			ADC_Result = ADC_GetConversionValue(ADC1);
	}
}

void BasicLSI302Test()
{
	LIS302DL_InitTypeDef init;
	InitTimer(TIM4);
	EnableGPIOClok(GPIOD);
	InitLedsPWM();
	SetTimer(TIM4, 1, 500);

	SetChannelPWM(TIM4, 1);
	SetChannelPWM(TIM4, 2);
	SetChannelPWM(TIM4, 3);
	SetChannelPWM(TIM4, 4);

	EnableTimerPreload(TIM4);

	EnableTimer(TIM4);

	SetPWM(TIM4, 3000, 4);

	init.Power_Mode = LIS302DL_LOWPOWERMODE_ACTIVE;
	init.Axes_Enable = LIS302DL_XYZ_ENABLE;
	init.Full_Scale = LIS302DL_FULLSCALE_9_2;
	init.Output_DataRate = LIS302DL_DATARATE_400;
	init.Self_Test = LIS302DL_SELFTEST_NORMAL;

	LIS302DL_Init(&init);

	uint8_t temp;

	LIS302DL_Read(&temp, LIS302DL_WHO_AM_I_ADDR, 1);

	if (temp != 0x3b)
		return;

	uint8_t Buffer[6];
	int TimingDelay = 500;
	int memsteststatus;



	  /* Wait until detecting all MEMS direction or timeout */
	while((memsteststatus == 0x00)&&(TimingDelay != 0x00))
	{
		LIS302DL_Read(Buffer, LIS302DL_OUT_X_ADDR, 4);
		xdata = ABS((int8_t)(Buffer[0]));
	    ydata = ABS((int8_t)(Buffer[2]));
	    /* Check test PASS condition */
	    if ((xdata > MEMS_PASSCONDITION) || (ydata > MEMS_PASSCONDITION))
	    {
	      /* MEMS Test PASS */
	      memsteststatus = 0x01;
	    }
	}

	uint8_t lstatex, lstatey, lstatez;
	uint8_t nstatex = 0, nstatey = 0, nstatez = 0;


	volatile int i = 1000;
	while(1)
	{
		LIS302DL_Read(Buffer, LIS302DL_OUT_X_ADDR, 6);

		lstatex = nstatex;
		lstatey = nstatey;
		lstatez = nstatez;


		nstatex = ABS((int8_t)(Buffer[0]));
		nstatey = ABS((int8_t)(Buffer[2]));
		nstatez = ABS((int8_t)(Buffer[4]));

		if (!i)
		{
		if (ABS(lstatex - nstatex) > THRESHOLD)
			xdata = nstatex;
		else
			xdata = 0;

		if (ABS(lstatey - nstatey) > THRESHOLD)
			ydata = nstatey;
		else
			ydata = 0;

		if (ABS(lstatez - nstatez) > THRESHOLD)
			zdata = nstatez;
		else
			zdata = 0;

		i = 100;
		}
		else
		{
			xdata = nstatex;
			ydata = nstatey;
			zdata = nstatez;

		}

		i--;
	    SetPWM(TIM4, xdata, 1);
	    SetPWM(TIM4, ydata , 2);
	    SetPWM(TIM4, zdata , 3);
	}
}


int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();

	BasicLSI302Test();

	return 0;
}

