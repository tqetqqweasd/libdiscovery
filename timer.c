#include "timer.h"


//rename it
void InitTimer(TIM_TypeDef* timer)
{
	if (timer == TIM3)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	if (timer == TIM4)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	//...
}

void SetTimer(TIM_TypeDef* timer, uint16_t prescaler, uint32_t interval)
{

	TIM_TimeBaseInitTypeDef init;

	init.TIM_CounterMode = TIM_CounterMode_Up;
	//1KHz timer input
	//APB1 is set for 1MHz
	//Setting it for 72MHz caused problems (dunno why)
	init.TIM_Prescaler = prescaler;
	init.TIM_Period = interval;
	init.TIM_RepetitionCounter = 0;
	init.TIM_ClockDivision = 0;

	TIM_TimeBaseInit(timer, &init);
}

void EnableUpdateInt(TIM_TypeDef* timer)
{
	TIM_ITConfig(timer, TIM_IT_Update , ENABLE);
}

FlagStatus CheckTimer(TIM_TypeDef* timer)
{
	return TIM_GetFlagStatus(timer, TIM_FLAG_Update);
}

void ResetTimer(TIM_TypeDef* timer)
{
	TIM_ClearFlag(timer, TIM_FLAG_Update);
}

void ClearTimerPendingBit(TIM_TypeDef* timer)
{
	TIM_ClearITPendingBit(timer, TIM_IT_Update);
}

void SetChannelPWM(TIM_TypeDef* timer, uint8_t channel)
{
	TIM_OCInitTypeDef init;

	init.TIM_OCMode = TIM_OCMode_PWM1;
	init.TIM_OutputState = TIM_OutputState_Enable;
	init.TIM_Pulse = 0;
	init.TIM_OCPolarity = TIM_OCPolarity_High;

	switch(channel)
	{
	case 1:
		TIM_OC1Init(timer, &init);
		TIM_OC1PreloadConfig(timer, TIM_OCPreload_Enable);
		break;
	case 2:
		TIM_OC2Init(timer, &init);
		TIM_OC2PreloadConfig(timer, TIM_OCPreload_Enable);
		break;
	case 3:
		TIM_OC3Init(timer, &init);
		TIM_OC3PreloadConfig(timer, TIM_OCPreload_Enable);
		break;
	case 4:
		TIM_OC4Init(timer, &init);
		TIM_OC4PreloadConfig(timer, TIM_OCPreload_Enable);
		break;

	}
}


void EnableTimer(TIM_TypeDef* timer)
{
	TIM_Cmd(timer, ENABLE);
}

void EnableTimerPreload(TIM_TypeDef* timer)
{
	TIM_ARRPreloadConfig(timer, ENABLE);
}

void SetPWM(TIM_TypeDef* timer, uint32_t value, uint8_t channel)
{
	switch (channel)
	{
	case 1: timer->CCR1 = value; break;
	case 2: timer->CCR2 = value; break;
	case 3: timer->CCR3 = value; break;
	case 4: timer->CCR4 = value; break;
	}
}

uint32_t GetPWM(TIM_TypeDef* timer, uint8_t channel)
{
	switch (channel)
		{
		case 1: return timer->CCR1;
		case 2: return timer->CCR2;
		case 3: return timer->CCR3;
		case 4: return timer->CCR4;
		}

	return 0;
}

void Delay(uint16_t ms)
{
	TIM_SetCounter(TIM3, 0);

	while(ms > TIM_GetCounter(TIM3));
}

void SetUpDelayTimer()
{
	InitTimer(TIM3);
	SetTimer(TIM3, 1000, 100000);
	EnableTimer(TIM3);
}
