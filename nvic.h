#pragma once
#include "dislib.h"

//helper for setting NVIC_InitTypeDefWhatevar
void SetupNVIC(FunctionalState fun, uint8_t channel, uint8_t preemption, uint8_t sub);

//Enables particular irq (exp. TIM3_IRQn)
void EnableIRQn(uint8_t irq);

void SetupEXTI(FunctionalState fun, uint32_t line, EXTIMode_TypeDef mode, EXTITrigger_TypeDef trigger);

void ConnectButtonToEXIT();
