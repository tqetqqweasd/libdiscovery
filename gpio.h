#pragma once

#include "dislib.h"

void InitLeds();
void InitLedsPWM();
void InitUserButton();
void SetLeds();
void ResetLeds();
uint8_t GetButtonState();
void SetBits(GPIO_TypeDef* port, uint16_t pins);
void SetPinAFConfig(GPIO_TypeDef* port, uint16_t pins, uint8_t af);
void EnableGPIOClok(GPIO_TypeDef* port);
