#pragma once

#include <stm32f4xx.h>
#include <stm32f4xx_syscfg.h>
#include <system_stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_exti.h>
#include <stm32f4xx_spi.h>
#include <stm32f4xx_tim.h>
#include <stm32f4xx_adc.h>
#include <stm32f4xx_dac.h>
#include <stm32f4xx_i2c.h>
#include <stm32f4xx_pwr.h>
#include <stm32f4_discovery_lis302dl.h>

#include <misc.h>

#include "gpio.h"
#include "timer.h"
#include "nvic.h"
