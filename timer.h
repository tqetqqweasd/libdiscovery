#pragma once
#include "dislib.h"

//using only internal clock, interval in ms
void SetTimer(TIM_TypeDef* timer, uint16_t prescaler, uint32_t interval);

//clock enabling
void InitTimer(TIM_TypeDef* timer);

//Checking UPDATE flag of a timer
FlagStatus CheckTimer(TIM_TypeDef* timer);

//Reseting timer UPDATE flag
void ResetTimer(TIM_TypeDef* timer);

//Enabling interrupts (UPDATE) for timer
void EnableUpdateInt(TIM_TypeDef* timer);

//Clears Timer Pending Bit (UPDATE)
void ClearTimerPendingBit(TIM_TypeDef* timer);

//Setting up timer in PWM mode
void SetTimerPWM(TIM_TypeDef* timer);

//Enables timer
void EnableTimer(TIM_TypeDef* timer);

//Enabling buffering
void EnableTimerPreload(TIM_TypeDef* timer);

//Setting timer channel to PWM
void SetChannelPWM(TIM_TypeDef* timer, uint8_t channel);

//sets timer CCR register value
void SetPWM(TIM_TypeDef* timer, uint32_t value, uint8_t channel);

//returns CCR value
uint32_t GetPWM(TIM_TypeDef* timer, uint8_t channel);

//sets up TIM3 as delay timer
void SetUpDelayTimer();

//TIM3 based delay
void Delay(uint16_t ms);
