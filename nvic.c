#include "nvic.h"

void SetupNVIC(FunctionalState fun, uint8_t channel, uint8_t preemption, uint8_t sub)
{
	NVIC_InitTypeDef init;

	init.NVIC_IRQChannelCmd = fun;
	init.NVIC_IRQChannel = channel;
	init.NVIC_IRQChannelPreemptionPriority = preemption;
	init.NVIC_IRQChannelSubPriority = sub;

	NVIC_Init(&init);
}

void EnableIRQn(uint8_t irq)
{
	NVIC_EnableIRQ(irq);
}

void ConnectButtonToEXIT()
{
	SYSCFG_EXTILineConfig(GPIOA, EXTI_PinSource0);
}

void SetupEXTI(FunctionalState fun, uint32_t line, EXTIMode_TypeDef mode, EXTITrigger_TypeDef trigger)
{
	EXTI_InitTypeDef EXTI_InitStructure;

	EXTI_InitStructure.EXTI_Line = GPIO_Pin_0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;

	EXTI_Init(&EXTI_InitStructure);
}
